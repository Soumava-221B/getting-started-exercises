# Go and GitLab Duo (AI)

Example solutions: https://gitlab.com/gitlab-da/use-cases/ai/ai-workshops/gitlab-duo-ai-eddie-jaoude/go-hello-duo 

## Ask Duo Chat how to get started

Open the [Duo Chat](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html) prompt, if not already open.

```markdown
How to start a Go project? Please share CLI commands, .gitignore, and more best practices.
```

Ask about the `go.mod` format.

```markdown
Show an example to build, test, run Go code in GitLab CI/CD pipelines
```

## Start writing code

Define a new project.

### Query a DNS resolver

```go
// Create a function which queries a DNS resolver for a given hostname
// Call the function in main with gitlab.com
```

### Create a webserver with Prometheus metrics

```go
// Create webserver
// Add Prometheus metrics endpoint
// Calculate some dummy metrics
```

## Learn about source code

Import an existing open source project, and let GitLab Duo explain and refactor the code.