# Challenges

## Duo Challenges

Fork the projects in https://gitlab.com/gitlab-da/use-cases/ai/ai-workflows/gitlab-duo-challenges (work-in-progress) and try to analyse and fix.

## Inspect prompts and re-create them

https://gitlab.com/gitlab-da/use-cases/ai/ai-workflows/gitlab-duo-prompts

https://docs.gitlab.com/ee/user/gitlab_duo_examples.html 