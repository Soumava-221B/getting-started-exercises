# Python and GitLab Duo (AI)

Example solutions: https://gitlab.com/gitlab-da/use-cases/ai/ai-workshops/gitlab-duo-ai-eddie-jaoude/python-hello-duo

## Ask Duo Chat how to get started

Open the [Duo Chat](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html) prompt, if not already open.

```markdown
How to get started with a Python project. Describe the project setup, .gitignore, CI/CD configuration.
```

Ask about the `requirements.txt` format.

```markdown
Please show an example for a requirements.txt format.
```

## Start writing code

Define a new project.

1. https://about.gitlab.com/blog/2023/11/09/learning-python-with-a-little-help-from-ai-code-suggestions/ 

### Create a Hello World application

You can ask Chat.

```markdown
Please generate code for a hello world application. Be creative and make it playful learning.
```

Or, change focus into a new file called `hello.py`.

Start new code comments with `#` and ask to generate the code again.

```python
# Create a hello world application
# Make it playful and fun to learn
```

### Create a web application

Create a new file called `server.py`. 

Add code comments at the top and create a webserver.

```python
# Create a webserver
```

Refine the prompt, and ask for Flask specifically.

```python
# Create a Flask webserver
# Use common URL endpoints
```

Refine the prompt to create a webserver with multiple URL endpoints.

```python
# Create a Flask webserver
# Add URL endpoints for 
# /hello
# /health
# /shop
# Run the webserver
```

Tip: You can also ask GitLab Duo to print the requirements.txt dependencies as code comment. Makes it easier to copy/paste them.

```python
# Create a Flask webserver
# Add URL endpoints for 
# /hello
# /health
# /shop
# Run the webserver
# Show the requirements.txt as code comment
```

### Explain code

Generated code is not always immediately clear. 

Select the generated source code, and right-click to use [GitLab Duo Chat to `/explain` the code](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#explain-code-in-the-ide). Alternatively, select the code, and type `/explain` in the prompt.

After the explanation, keep the code selected, and refine the `/explain` prompt to

```markdown
/explain with focus on the algorithms
```

### Generate Tests

How to test a webserver with mocked endpoints?

Lets ask Duo Chat first.

```markdown
Please show an example for testing a Flask webserver application code.
```

The response might lack context of the existing source.

Try a different route, and select the generated functions, and use either right-click, GitLab Duo Chat, Generate Tests - or the `/tests` slash command in the Chat prompt. https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#write-tests-in-the-ide 

Tip: Refine the prompt to focus on specific extreme test cases, or using a different test framework.

### Refactor code

Select the source code and use the `/refactor` slash command.

https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#refactor-code-in-the-ide 


### Explain and fix vulnerabilities

AI-generated source code might still introduce security problems, similar to humans writing code.

The following code allows an attacker to inject arbitrary OS commands via the 'query' parameter. The `os.system()` call executes those commands directly without any sanitization or validation.

```python
import flask
from flask import request

app = flask.Flask(__name__)

@app.route('/search')
def search():
    query = request.args.get('query') 
    execute_os_command(query)
    return 'You searched for: ' + query

def execute_os_command(command):
    os.system(command)

if __name__ == '__main__':
    app.run()
```

Add the code into a new file called `vuln-flask.py`.

Configure SAST scanning in GitLab (requires Ultimate tier) by adding a `.gitlab-ci.yml` file and add the [SAST CI/CD component](https://gitlab.com/explore/catalog/components/sast):

```yaml
include:
  - component: gitlab.com/components/sast/sast@main
```

Commit, push and merge the changes.

Navigate into `Secure > Vulnerability Report` to inspect the detected vulnerabilities.

1. Select to explain the OS command injection vulnerability.
1. Resolve the vulnerability and inspect the Merge Request Diff
    - Example in https://gitlab.com/gitlab-da/use-cases/ai/ai-workshops/gitlab-duo-ai-eddie-jaoude/python-hello-duo/-/merge_requests/1 

Project path (project access, and developer role required): https://gitlab.com/gitlab-da/use-cases/ai/ai-workshops/gitlab-duo-ai-eddie-jaoude/python-hello-duo/-/security/vulnerability_report

## Advanced

### Fun: Query the Chuck Norris API

```python
# Create a function that queries the Chuck Norris API and prints its output
```



### Add client-side templates

```python

```

### Add JavaScript



## Learn about source code

Import an existing open source project, and let GitLab Duo explain and refactor the code.